const Weekdays = ['Mon','Tue','Wed','Thu','Fri'];
const Weekends = ['Sat','Sun'];

let Id = 1;

export function nextId() { return Id++; }

export function summariseDays(dayStr = '') {
  const days = new Set(dayStr.split(/\s*,\s*/g));
  if ( days.size == 7 ) return 'Every day';
  if ( Weekdays.every(day => days.has(day))) return 'Week days';
  if ( Weekends.every(day => days.has(day))) return 'Weekends';
  return dayStr;
}

