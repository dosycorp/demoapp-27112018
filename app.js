import {App,BikersTable} from './views.js';
import {nextId, summariseDays} from './utils.js';

const appState = {
  appName: 'Dummy App',
  appVersion: '1.14',
  homeLink: '/',
  days: [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ],
  userName: 'John',
  logoutLink: '#logout',
  sitemap: {
    page1: {
      title: 'Page 1',
      href: '#page-1'
    },
    breadcrumb: {
      title: 'Breadcrumb',
      href: '#breadcrumb'
    }
  },
  currentPage: {
    title: 'Current Page',
    data: {
      regularSigns: [
        {
          header: 'Sector',
          byline: 'Sports',
          icon: 'puzzle-piece',
        },
        {
          header: 'Sport Type',
          byline: 'Bikes',
          icon: 'futbol',
        },
        {
          header: 'Mode',
          byline: 'Mountain Cross',
          icon: 'bicycle'
        },
      ],
      highlitSigns: [
        {
          header: 'Responsibility',
          byline: 'Be Green!',
          icon: 'recycle'
        },
      ]
    }
  },
  help: {
    paragraphs: [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ]
  },
  bikers: {
    columns: [
      { title: 'Full name', prop: 'full_name' },
      { title: 'Email', prop: 'email' },
      { title: 'City', prop: 'city' },
      { title: 'Ride in group', prop: 'groupup' },
      { title: 'Days of the week', prop: 'godays' , transform: summariseDays },
      { title: 'Registration day', prop: 'created_at', transform: v => (new Date(v)).toLocaleString()}
    ],
    rows: [
      {
        id: nextId(),
        full_name: 'James Isaac Neutron',
        email: 'neutron@example.com',
        city: 'City',
        groupup: 'Always',
        godays: 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
        created_at: Date.now(),
      },
      {
        id: nextId(),
        full_name: 'Carl Wheezer',
        email: 'carl@example.com',
        city: 'City',
        groupup: 'Sometimes',
        godays: 'Mon,Tue,Wed,Thu,Fri',
        created_at: Date.now(),
      },
      {
        id: nextId(),
        full_name: 'Cindy Vortex',
        email: 'cindyvortex@example.com',
        city: 'City',
        groupup: 'Never',
        godays: 'Sat,Sun',
        created_at: Date.now(),
      },
      {
        id: nextId(),
        full_name: 'Sheen Estevez',
        email: 'sheen@example.com',
        city: 'City',
        groupup: 'Sometimes',
        godays: 'Mon,Wed,Fri',
        created_at: Date.now(),
      },
      {
        id: nextId(),
        full_name: 'Libby Folfax',
        email: 'folfax2014@example.com',
        city: 'City',
        groupup: 'Sometimes',
        godays: 'Mon,Tue,Wed',
        created_at: Date.now(),
      },
      {
        id: nextId(),
        full_name: 'Nick Dean',
        email: 'nickd@example.com',
        city: 'City',
        groupup: 'Always',
        godays: 'Fri,Sat',
        created_at: Date.now(),
      },
    ]
  }
};

App(appState).to('main.app', 'innerHTML');

