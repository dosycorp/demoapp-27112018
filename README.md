# demoapp-27112018

To run clone the repo and then:

```
  $ ./install.sh
  $ npm test
```

Then proceed to [http://localhost:8080](http://localhost:8080) in a browser.

## Notes

Tested to work in latest Chrome, Firefox, Edge, and mobile Safari.

## Improvements and possible changes

The things I think I could improve or change:

- Refactor CSS classes and code to make more modular, and extensible. 
- More perfectly match colors of table, button, link and outlines.
- Style radio and checkboxes to match PDF 
 - For example round/colored outlines/borders on hover/focus and selection/checking.

## Rationale

My rationale in doing it this way (and not going further) was to keep everything as simple as possible. Least amount of markup, JS, and CSS. Just to keep everything simple as a demonstration but still reach key functionality. 

Also the time I spent on it (a couple hours, maybe 3 - 4 in total), felt like a good enough effort for this stage to demonstrate relevant skills.

## Thanks

Thank you for this opportunity and your time in considering.







