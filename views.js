import {R,X} from './node_modules/brutalist-web/r.js';
import {nextId} from './utils.js';

export function App(S) {
  return R`
    ${AppHeader(S)}
    ${PageContent(S)}
    ${AppFooter(S)}
  `;
}

export function BikersTable(S) {
  return R`
    <table>
      <thead>
        <tr>
          ${S.bikers.columns.map(c => X`<th>${c.title}</th>`)}
          <th></th>
        </tr>
      </thead>
      <tbody>
        ${S.bikers.rows.map(biker => X`
          <tr data-id=${biker.id}>
            ${S.bikers.columns.map(({prop, transform: transform = v => v})=> X`
              <td>${transform(biker[prop])}</td>      
            `)}
            <td class=control>
              <button click=${() => removeRow(biker.id, S)} class="delete icon-only">
                <i class="fa fa-trash-alt"></i>
              </button>
            </td>
          </tr>
        `)}
      </tbody>
    </table>
  `;
}

function AppHeader(S) {
  return R`
    <header>
      <section class="menu sticky">
        ${BurgerMenu(S)}
        ${AuthMenu(S)}
      </section>
      <section class="menu">
        ${Breadcrumbs(S)}
      </section>
      <section class="info signbar">
        ${Signbar(S)}
      </section>
    </header>
  `;
}

function AppFooter(S) {
  return R`
    <footer>
      <span class=app-name>${S.appName}</span> 
      <span class=app-version>${S.appVersion}</span>
    </footer>
  `;
}

function PageContent(S) {
  return R`
    <section class=page-content>
      <article class=help-box>
        ${Help(S)} 
      </article>
      <article class=user-registration>
        <h1>User Registration</h1>
        ${UserRegistration(S)}
      </article>
      <article class=bikers-table>
        <h1>Bikers</h1>
        ${BikersTable(S)}
      </article>
    </section>
  `;
}

function UserRegistration(S) {
  return R`
    <form class=user-registration submit=${s => addRow(s, S)}>
      <fieldset>
        <p>
          <label>
            <em>Full name</em>
            <input required type=text name=full_name>
            <small class="hidden info">Instructions to show on input focus.</small>
          </label>
        <p>
          <label>
            <em>Email</em>
            <input required type=email name=email>
            <small class="hidden info">Instructions to show on input focus.</small>
          </label>
        <p>
          <label>
            <em>City</em>
            <small class="importance optional">optional</small>
            <input required type=text name=city>
            <small class="hidden info">Instructions to show on input focus.</small>
          </label>
      </fieldset>
      <fieldset>
        <p>
          <em>Ride in a group?</em>
          <br>
          <label>
            <input type=radio name=groupup value=Always>
            Always
          </label>
          <label>
            <input type=radio name=groupup value=Sometimes>
            Sometimes
          </label>
          <label>
            <input type=radio name=groupup value=Never>
            Never
          </label>
        <p>
          <em>Days of the week</em>
          <br>
          ${S.days.map(d => X`
            <label>
              <input name=godays value=${d} type=checkbox>
              ${d}
            </label>
          `)}
        <p class=tx-rt>
          <button type=reset>Cancel</button>
          <button type=submit>Save</button>
      </fieldset>
    </form>
  `;
}

function Help(S) {
  return R`
    <details>
      <summary>
        <strong>Help</strong>
        <button class="icon-only close keylit">close <i class="fa fa-long-arrow-alt-up"></i></button>
      </summary>
      <article class=help>
        <div class="icon">
          <i class="fa fa-life-ring"></i>
        </div>
        ${S.help.paragraphs.map(p => X`<p>${p}</p>`)}
      </article>
    </details>
  `;
}

function Sign(S, {classNames: classNames = []} = {}) {
  return X`
    <article class="sign ${classNames.join(' ')}">
      <section class="icon">
        <i class="fa fa-${S.icon}"></i>
      </section>
      <section class=text>
        <h1>${S.header}</h1>
        <p>${S.byline}</p>
      </section>
    </article>
  `;
}

function Signbar(S) {
  return R`
    <ul class="regular signs">
      ${S.currentPage.data.regularSigns.map(s => X`<li>${Sign(s)}</li>`)}
    </ul>
    <ul class="highlit signs">
      ${S.currentPage.data.highlitSigns.map(s => X`<li>${Sign(s, {classNames: ["sign-keylit"]})}</li>`)}
    </ul>
  `;
}

function BurgerMenu(S) {
  return R`
    <nav class="burger">
      <i class="fa fa-bars"></i>
      <h1 class=app-name>${S.appName}</h1>
      <ul></ul>
    </nav>
  `;
}

function AuthMenu(S) {
  return R`
    <nav class="auth">
      <i class="fa fa-user keylit"></i>
      <span class=user-name>${S.userName}</span>
      |
      <a href=${S.logoutLink} class=logout>Log out</a>
    </nav>
  `;
}

function Breadcrumbs(S) {
  return R`
    <nav class="crumbs">
      <ul> 
        <li class=home>
          <a href=${S.homeLink}>
            <i class="fa fa-home"></i>
          </a>
        </li>
        <li>
          <a href=${S.sitemap.page1.href}>
            ${S.sitemap.page1.title}
          </a>
        </li>
        <li>
          <a href=${S.sitemap.breadcrumb.href}>
            ${S.sitemap.breadcrumb.title}
          </a>
        </li>
        <li class=current-page>
          ${S.currentPage.title}
        </li>
      </ul>
    </nav>
  `;
}

function addRow(submission, S) {
  // This is much shorter if FormData methods are supported, but edge and Safari so far do not
  const {target:form} = submission;
  submission.preventDefault();
  const newRow = S.bikers.columns.reduce((D,{prop}) => {
    let values = form[prop];
    if ( !! values ) {
      if (values instanceof NodeList || values instanceof HTMLCollection ) {
        values = Array.from(values).filter(el => el.checked || el.selected );
        values = values.map(el => el.value);
        values = values.join(',');
      } else {
        values = values.value;
      }
    }
    D[prop] = values;
    return D;
  }, {});
  newRow.id = nextId();
  newRow.created_at = Date.now();
  console.log(newRow);
  S.bikers.rows.push(newRow);
  BikersTable(S);
}

function removeRow(removeId, S) {
  const goAhead = confirm("Delete this row?");
  if ( goAhead ) {
    const removedRowIndex = S.bikers.rows.findIndex(({id}) => id == removeId);
    if ( removedRowIndex !== -1 ) {
      S.bikers.rows.splice(removedRowIndex, 1);
      BikersTable(S);
    } else {
      throw new TypeError(`Asking to remove row of biker with id ${removeId} but that id not found.`);
    }
  }
}
